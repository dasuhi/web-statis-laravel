<?php

use Illuminate\Support\Facades\Route;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

    Route::get('/', 'HomeController@index');
    Route::get('/form', 'AuthController@form');

    Route::post('/welcome', 'AuthController@welcome');

    Route::get('/master', 'TableController@master' );

    Route::get('/table', 'TableController@table');
    Route::get('/data-tables', 'TableController@datatables' );


    // Cast Controller
    Route::get('/cast', 'CastController@index' );
    Route::get('/cast/create', 'CastController@create' );
    Route::post('/cast', 'CastController@store' );
    Route::get('/cast/{cast_id}', 'CastController@show' );
    Route::get('/cast/{cast_id}/edit', 'CastController@edit' );
    Route::put('/cast/{cast_id}', 'CastController@update' );
    Route::delete('/cast/{cast_id}', 'CastController@destroy' );