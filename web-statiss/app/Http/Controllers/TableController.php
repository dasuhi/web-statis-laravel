<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function master() 
    {
        return view('master');
    }

    public function dataTables()
    {
    
        return view('pages.datatables');
    }

    public function table()
    {
       

        return view('pages.table');
    }
}
