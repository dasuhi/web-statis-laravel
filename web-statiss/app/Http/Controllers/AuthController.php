<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('pages.register');
    }

    public function welcome(Request $request)
    {
        $namaAwal = $request->firstName;
        $namaAkhir = $request->lastName;

        return view('pages.welcome', compact('namaAwal', 'namaAkhir'));
    }


}
