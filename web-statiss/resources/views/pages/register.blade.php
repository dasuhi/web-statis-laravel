   @extends('master')
   
   



   @section('form')

   <div class="container">
    <h1>Buat Account Bergabung!</h1>

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <p>
            <label for="firstName">First name:</label><br><br>
            <input type="text" name="firstName"/>
        </p>
        
        <p>
            <label for="lastName">Last name:</label><br><br>
            <input type="text" name="lastName"/>
        </p>
 
        <p>
        <label for="gender">Gender:</label>
            <br>
            <label for=""><input type="radio" name="Male" id="" value="male"> Male</label>
            <br>
            <label for=""><input type="radio" name="Female" id="" value="female"> Female</label>
            <br>
            <label for=""><input type="radio" name="Other" id="" value="other"> Other</label>
            <br>
        </p>
        <p>
            <label for="nationality">Nationality:</label><br><br>
            <select name="" id="">
                <option value="Japan">Japan</option>
                <option value="Indonesia">Indonesia</option>
            </select>
        </p>
        <p>
        <label for="Language">Language Spoken:</label><br><br>
            <label for="bahasa-indo"><input type="checkbox" name="bahasa-indo">Bahasa Indonesia</label>
            <br>
            <label for="english"><input type="checkbox" name="english">English</label>
            <br>
            <label for="other-lang"><input type="checkbox" name="other-lang">Other</label>
        </p>
        <p>
            <label for="bio">Bio:</label><br><br>
            <textarea name="" id="" cols="30" rows="10"></textarea>
        </p>
        <p>
            <input type="submit" value="Sign Up" name="signup">
        </p>
 
    </form>
   </div>
   


   @endsection
    