@extends('master')
@section('title')
    <h2>View</h2>
@endsection

@section('view')
<div class="container">
    <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
                <tr>
                    <td>{{$cast->id}}</th>
                    <td>{{$cast->name}}</td>
                    <td>{{$cast->umur}}</td>
                    <td>{{$cast->bio}}</td>
                    <td>
                </tr>
                <tr colspan="3">
                    <td>No data</td>
                </tr>              
        </tbody>
    </table>
</div>

@endsection